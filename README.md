# ComplexBrowser

Welcome to the repository of ComplexBrowser!

For more details see the manuscript: https://doi.org/10.1074/mcp.TIR119.001434

See the [Tutorial](https://bitbucket.org/michalakw/complexbrowser/raw/dcaef828f7f0f9501c4f041391471b94acf00be1/Manual.pdf) for a detailed explanation of the tool.

You can 

- Run the app via http://computproteomics.bmb.sdu.dk/Apps/ComplexBrowser
- Install the app on your computer and run it e.g. in RStudio
- Run the docker image veitveit/complexbrowser

Find the latest released versions here: https://bitbucket.org/michalakw/complexbrowser/downloads/?tab=tags

Or download the source code of the newest version: https://bitbucket.org/michalakw/complexbrowser/downloads/?tab=downloads

